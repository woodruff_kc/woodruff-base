<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    //site name
    public function siteName()
    {
        return get_bloginfo('name');
    }
    //universal title
    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }
    
}
/**
 * Convert string for use as title/alt tags
 */
function convertTitle($title){
    $title = strip_tags($title); // Strip all tags
    $title = str_replace('\"', '', $title);
    return $title;
}

