export default {
  init() {
    // JavaScript to be fired on all pages
    $('select').parent().addClass('select-wrapper');

    $('form.searchandfilter select').on('change', function() {
      var $form = $(this).closest('form');
      $form.find('input[type=submit]').click();
    });
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
