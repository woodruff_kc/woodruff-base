{{--
  Template Name: Home Page
--}}
@extends('layouts.app')

@section('content')
  <div id="date">
  @php
      $today = date("M j");
      echo $today;
  @endphp
  </div>
  @include('partials.page-header')
  does this do anything ?
  
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif
    @php $i = 0; @endphp
  @while (have_posts()) @php the_post() @endphp
    @php 
    $id = get_the_ID();
    $status = get_field('change_order_status', $id);
    $changeOrder = get_field('change_order', $id);
    $brandName = $changeOrder['brand_name'];
    $formualName = $changeOrder['formula_name'];
    $secondaryName = $changeOrder['secondary_name'];
    $size = $changeOrder['size'];
    $sku = $changeOrder['sku'];
    $skuChangeNote = $changeOrder['sku_change_note'];
    $dateCreated = $changeOrder['date_created'];
    $requestedBy = $changeOrder['requested_by'];
    $fullUPC = $changeOrder['full_upc'];
    $brandId = $changeOrder['brand_id'];
    $formulaId = $changeOrder['formula_id'];
    $changeOrderClasses = $status.' changed-order';
    @endphp

    <article @php post_class($changeOrderClasses) @endphp>
      <div class="change-order-content">
        <header>
          <h2 class="entry-title change-order-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
          <div class="change-order-status-form">
              @php // update the change order form
              acf_form(array(
                  'post_id'		=> get_the_id(),
                  'post_title'	=> false,
                  'post_content'	=> false,
                  'form' => true,
                  'new_post'		=> array(
                      'post_type'		=> 'change-order',
                      'post_status'	=> 'publish'
                  ),
                  'fields' => array('field_5c919b252d0ad'),
                  'return' => '/change-orders',
                  'submit_value'       => 'Update Status',
                  'updated_message'    => 'Updated!'
                   // Redirect to new post url
              ));
              @endphp
            </div>
        </header>
        <div class="change-order-details">
          
        </div>
      </div>
    </article>
    
    
  @endwhile

  {!! get_the_posts_navigation() !!}
@endsection
