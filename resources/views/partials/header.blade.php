<header class="banner">
  <div class="logos">
    <img src="@asset('images/logo-woodruff-light.png')" alt="Woodruff Logo"/>
    <img src="@asset('images/logo-diamond.png')" alt="Diamond Logo"/>
    <a class="brand btn" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
  </div>
  <nav class="nav-primary">
    @if (has_nav_menu('primary_navigation'))
      {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
    @endif
  </nav>
  @php dynamic_sidebar('sidebar-nav') @endphp
</header>
