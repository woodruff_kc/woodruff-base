<footer class="content-info">
  <div class="footer-widgets">
    @php dynamic_sidebar('sidebar-footer') @endphp
  </div>
</footer>
