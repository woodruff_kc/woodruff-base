<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    <div id="siteWrap">
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="content-wrap" role="document">
      <div class="content">
        <main class="main">
          @yield('content')
        </main>
      </div><!-- end .content -->
    </div><!-- end .wrapper -->
    @if (App\display_sidebar())
      <aside class="sidebar">
        @include('partials.sidebar')
      </aside>
    @endif
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
    </div> <!-- end #siteWrap -->
  </body>
</html>
